import { Injectable } from "@angular/core";
import { Client } from "elasticsearch-browser";
import * as elasticsearch from "elasticsearch-browser";
import axios from "axios";
import _ from "lodash";

@Injectable({
  providedIn: "root"
})
export class ElasticsearchService {
  private client: Client;

  constructor() {
    if (!this.client) {
      this._connect();
    }
  }

  private connect() {
    this.client = new Client({
      host: "https://mmmmm",
      log: "trace"
    });
  }

  private _connect() {
    this.client = new elasticsearch.Client({
      host:
        "search-trlm-tika-poc-ikhcw3ie6kko6jzvot7nxwzdzq.us-east-1.es.amazonaws.com",
      log: "trace"
    });
  }

  isAvailable(): any {
    return this.client.ping({
      requestTimeout: Infinity,
      body: "hello grokonez!"
    });
  }

  addToIndex(value: any): any {
    return this.client.create(value);
  }

  private queryalldocs = {
    query: {
      match_all: {}
    }
  };

  getAllDocuments(_index: any, _type: any): any {
    return this.client.search({
      index: _index,
      type: _type,
      body: this.queryalldocs,
      filterPath: ["hits.hits._source"]
    });
  }

  getAllDocumentsWithScroll(_index: any, _type: any, _size: any): any {
    return this.client.search({
      index: _index,
      type: _type,
      // Set to 1 minute because we are calling right back
      // (Elasticsearch keeps the search context open for another 1m)
      scroll: "1m",
      filterPath: ["hits.hits._source", "hits.total", "_scroll_id"],
      body: {
        size: _size,
        query: {
          match_all: {}
        },
        sort: [{ _uid: { order: "asc" } }]
      }
    });
  }

  getNextPage(scroll_id: any): any {
    return this.client.scroll({
      scrollId: scroll_id,
      scroll: "1m",
      filterPath: ["hits.hits._source", "hits.total", "_scroll_id"]
    });
  }

  fullTextSearch(_index: any, _type: any, _field: any, _queryText: any): any {
    return this.client.search({
      index: _index,
      type: _type,
      // filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      body: {
        query: {
          match_phrase_prefix: {
            [_field]: _queryText
          }
        }
      },
      //explain: true,
      _source: ["name", "empId", "objectUrl"]
    });
  }

  fullTextSearchContent(
    _index: any,
    _type: any,
    _field: any,
    _queryText: any
  ): any {
    return this.client.search({
      index: _index,
      type: _type,
      // filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      body: {
        query: {
          match: {
            [_field]: _queryText
          }
        }
      },
      //explain: true,
      _source: ["name", "empId", "objectUrl", "content"]
    });
  }

  async fullTextSearchContentdistro(
    _index: any,
    _type: any,
    _field: any,
    _queryText: any
  ) {
    let sql = 'SELECT * FROM  thz_indexv1 where empId="888888" LIMIT 1';
    console.log("gggggggggg", sql);

    let q1 = {
      body: {
        query: sql
      }
    };

    console.log("gggggggggg", q1);

    let q = {
      body: {
        query: {
          match_phrase_prefix: {
            [_field]: _queryText
          }
        },
        _source: ["name", "empId", "objectUrl"]
      }
    };

    console.log("HHHHHHHHHH", q);

    let u = await axios
      .post(
        "http://search-trlm-tika-poc-ikhcw3ie6kko6jzvot7nxwzdzq.us-east-1.es.amazonaws.com/_opendistro/_sql",
        {
          query: "SELECT * FROM  thz_indexv1 LIMIT 1"
        }
      )
      .then(res => {
        console.log("ggggggggggggggg", res);
      });

    console.log("Finished");
    let tempArr = [];
    let mapping = await axios.get(
      "http://search-trlm-tika-poc-ikhcw3ie6kko6jzvot7nxwzdzq.us-east-1.es.amazonaws.com/thz_indexv1/_mapping"
    );
    let op = mapping["data"]["thz_indexv1"]["mappings"]["properties"];

    for (let k in op) {
      console.log("HHHHHHH>>>>", op[k]["type"] + "<<>>" + k);
      if (op[k]["type"] === "text") {
        tempArr.push(k);
      }
    }
    // .then(res => {
    //   console.log(
    //     "pppppppp",
    //     res["data"]["thz_indexv1"]["mappings"]["properties"]
    //   );
    //   let op = res["data"]["thz_indexv1"]["mappings"]["properties"];

    //   for (let k in op) {
    //     console.log("HHHHHHH>>>>", op[k]["type"] + "<<>>" + k);
    //     if (op[k]["type"] === "text") {
    //       tempArr.push(k);
    //     }
    //   }
    // });
    console.log("Finished", tempArr);

    return this.client.search({
      index: _index,
      type: _type,
      // filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      // body: {
      //   query: sql
      // },
      body: {
        query: {
          match: {
            [_field]: _queryText
          }
        }
      },
      //explain: true,
      _source: ["name", "empId", "objectUrl", "content"]
    });
  }

  async oktaTest() {
    let api = "00ptnIV3Kz1o9thpNPlolj43qnTL4b-c9oTZhktWcS";
    let config = {
      headers: {
        Authorization: `SSWS 00ptnIV3Kz1o9thpNPlolj43qnTL4b-c9oTZhktWcS`,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    let resp = axios.get(
      "https://deloitte-npanugantivenkata.okta.com/api/v1/users?limit=25",
      config
    );
  }
}

import { Injectable } from "@angular/core";
import WebSocket from "isomorphic-ws";

@Injectable({
  providedIn: "root",
})
export class WebsocketService {
  private websocket: any;
  messageListeners: any;
  private isOpen: any;

  constructor() {
    console.log("Establishing websocket connection");
    console.log(this.websocket);
    if (!this.websocket) {
      this.initSocket();
    }
  }

  /**
   *  Show connection status to us in the log
   */
  onConnOpen = (data: any) => {
    console.log(data);
    this.isOpen = true;
    console.log("Websocket connected!");
    this.updateuserId();
  };

  /**
   *  Log lost connection for now
   */
  onConnClose = () => {
    console.log("Websocket closed!");
  };

  /**
   * Handler that receives the actual messages from the WebSocket API
   * For now it simply returns the parsed message body to the appropriate
   * registered handler
   * @param data Message body received from WebSocket
   */
  onMessage = (data: any) => {
    console.log("Message Received>", data);
    if (data.data === "pong") {
      console.log("Pong for Ping");
    } else {
      const message = JSON.parse(data.data);
      console.log(message);
      if (message.userId === "manojm95@gmail.com") {
        console.log("Message for me!!!");
      }
    }

    // if (data) {
    //   const message = JSON.parse(data.data);
    //   const typeListener = this.messageListeners.find(
    //     (listener) => listener.type === message.type
    //   );

    //   if (typeListener && typeof typeListener.listener === "function") {
    //     typeListener.listener(message);
    //   } else {
    //     console.log("No handler found for message type");
    //   }
    // }
  };

  initSocket = () => {
    this.websocket = new WebSocket(
      "wss://ekr148mxwc.execute-api.us-east-1.amazonaws.com/dev"
    );
    this.websocket.onopen = this.onConnOpen;
    this.websocket.onmessage = this.onMessage;
    this.websocket.onclose = this.onConnClose;
  };

  ping = () => {
    if (this.websocket && this.isOpen) {
      this.websocket.send(
        JSON.stringify({
          action: "ping",
          message: JSON.stringify("Ping"),
        })
      );
    } else {
      console.log(`Websocket connection not found!!`);
    }
  };

  updateuserId = () => {
    if (this.websocket && this.isOpen) {
      this.websocket.send(
        JSON.stringify({
          action: "ping",
          message: JSON.stringify("manojm95@gmail.com"),
        })
      );
    } else {
      console.log(`Websocket connection not found!!`);
    }
  };
}

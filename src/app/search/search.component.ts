import { Component, OnInit } from "@angular/core";
import { ElasticsearchService } from "../elasticsearch.service";
import { WebsocketService } from "../websocket.service";
import axios from "axios";

export interface Employee {
  name: string;
  empId: string;
  url: string;
}

export interface EmployeeSource {
  source: Employee;
}

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.css"],
})
export class SearchComponent implements OnInit {
  private static readonly INDEX = "thz_indexv1";
  private static readonly TYPE = "_doc";

  headElements: string[] = ["#", "Emp Name", "Emp Id", "Resume"];

  employees: EmployeeSource[];

  private queryText = "";

  private lastKeypress = 0;

  asideVisible: boolean;

  constructor(private es: ElasticsearchService, private ws: WebsocketService) {
    this.queryText = "";
    this.asideVisible = this.ws.messageListeners;
  }

  ngOnInit() {
    console.log("Entering MMMM");

    this.es
      .oktaTest()
      .then((res) => console.log("MMMMM respoinse--->", res))
      .catch((err) => console.log("MMMM ERR", err));
  }

  search($event) {
    if ($event.timeStamp - this.lastKeypress > 100) {
      this.queryText = $event.target.value;

      if (this.queryText === "z") {
        this.ws.ping();
      } else {
        this.es
          .fullTextSearchContentdistro(
            SearchComponent.INDEX,
            SearchComponent.TYPE,
            "content",
            this.queryText
          )
          .then(
            (response) => {
              this.employees = response.hits.hits;

              // console.log(
              //   "MMMMM",
              //   response.hits.hits.map((x: any) => x._source.objectUrl)
              // );
            },
            (error) => {
              console.error(error);
            }
          )
          .then(() => {
            console.log("Search Completed!");
          });
      }
    }

    this.lastKeypress = $event.timeStamp;
  }

  async downloadObject(objUrl) {
    let name = objUrl.split("/").pop();
    const uploadUrl = await axios.post(
      "https://d7vgjq4jy3.execute-api.us-east-1.amazonaws.com/dev/preurl",
      {
        fileName: name,
        name: "getRequest",
        empId: "dummy",
      }
    );
    let finalUrl = uploadUrl["data"]["body"]["url"];
    var win = window.open(finalUrl, "_blank");
    win.focus();
  }
}

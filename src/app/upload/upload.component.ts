import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import axios from "axios";
import SSS from "smartystreets-javascript-sdk";
const SSC = SSS.core;
const Lookup = SSS.usStreet.Lookup;
var websiteKey = new SSC.SharedCredentials("27368713246582852");
var clientBuilder = new SSC.ClientBuilder(websiteKey);
var client = clientBuilder.buildUsStreetApiClient();
// let authId = "da46401d-1235-c0b4-4fb4-a0ebf05298c1";
// let authToken = "lPN6qJ9OASv6jRsPJcDZ";
// const credentials = new SSC.StaticCredentials(authId, authToken);
// let client = SSC.buildClient.usStreet(credentials);
let lookup1 = new Lookup();
lookup1.inputId = "24601"; // Optional ID from your system
lookup1.addressee = "John Doe";
lookup1.street = "11308 Plantation Lakes Circle";
lookup1.street2 = "";
lookup1.secondary = "";
lookup1.urbanization = ""; // Only applies to Puerto Rico addresses
lookup1.city = "Sanford";
lookup1.state = "Florida";
lookup1.zipCode = "32771";
lookup1.maxCandidates = 3;
lookup1.match = "invalid";

// let lookup2 = new Lookup();
// lookup2.street = "1600 Amphitheater Pkwy";
// lookup2.lastLine = "Mountainview, CA";
// lookup2.maxCandidates = 5;

// let lookup3 = new Lookup();
// lookup3.inputId = "8675309";
// lookup3.street = "1600 Amphitheatre Parkway Mountain View, CA 94043";
// lookup3.maxCandidates = 1;
let batch = new SSC.Batch();
batch.add(lookup1);
// batch.add(lookup2);
// batch.add(lookup3);

@Component({
  selector: "app-upload",
  templateUrl: "./upload.component.html",
  styleUrls: ["./upload.component.css"]
})
export class UploadComponent implements OnInit {
  param1: string;
  param2: string;
  constructor(private route: ActivatedRoute) {}

  name: string;
  empId: string;
  filePath: string;
  fileType: string;
  fileContent: object;
  isComplete: boolean = false;

  async ngOnInit() {
    client
      .send(batch)
      .then(res => {
        console.log("USSSSMMMM", res);
      })
      .catch(err => console.log("ERRRR", err));

    console.log("Called Constructor");
    const firstParam: string = this.route.snapshot.queryParamMap.get(
      "activationToken"
    );
    console.log("MMMMMKKKK", firstParam);
  }

  saveName(event) {
    console.log(event.target.value);
    this.name = event.target.value;
  }

  saveId(event) {
    console.log(event.target.value);
    this.empId = event.target.value;
  }

  saveFileName(event) {
    console.log("MMMMMM", event.target.files[0]);
    console.log("The vale is ", event.target.files[0]);
    this.filePath = event.target.files[0].name;
    this.fileType = event.target.files[0].type;
    this.fileContent = event.target.files[0];
  }

  async onSubmit() {
    console.log(
      "The recorded values are ",
      this.name + "  " + this.empId + " " + this.filePath + " " + this.fileType
    );
    console.log("nnnnnnnn", this.fileContent["type"]);
    this.isComplete = false;
    const uploadUrl = await axios.post(
      "https://d7vgjq4jy3.execute-api.us-east-1.amazonaws.com/dev/preurl",
      {
        fileName: this.fileContent["name"],
        name: this.name,
        empId: this.empId
      }
    );
    try {
      var options = {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        onUploadProgress: (progressEvent: ProgressEvent) => {
          let progress = (progressEvent.loaded / progressEvent.total) * 100;
          console.log("NNNNN", progress);
        }
      };
      let res = await axios.put(
        uploadUrl.data["body"]["url"],
        //"https://fda-trlm-poc.s3.amazonaws.com/public/products/abc%40gmail.com/1-13-2020-19%3A18%3A44/test.txt?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIA56SCV4ZON3AQYTOY%2F20200113%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200113T191844Z&X-Amz-Expires=1000&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEMT%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJIMEYCIQDhtuzDv%2FaJWgy60m4EtJ5wRZmu8I%2Bb2VQC0SFtMmpCCgIhAMc1r9Y6kuovW09mk6lO0Es3HAg9RqsoiuITrfdsGwkPKssBCEwQABoMOTU4OTkxMjk2MDkyIgxxMeVj8VEJUnaWopkqqAGtjG0ampDP2z%2FV07ajSEaH8xOKAIbKlMenLiNX8rhqnOMOAGhPD6GI5lJyR0%2FPs67wROkBW5F4W0eC0YVxx1vJMApCHD%2FiztOWlUwBUe77ERNX2k0irdP1HApYXx%2Bb60NaN4yChmqtWYwJjhPBJvAxM4MNHzMiaPb69IxzfDHVQBQ221AjSx7CRrCp1%2FnGttA1ZTvpBDQ6KuhrQDSiTMBXT%2Fqk1Tn%2Fircwh4Xz8AU63wE13f2UUjK9o5bt2vcyaWNs3j5fIORudIk%2BX5yn0H8u49S8VGthLhvGUUH2Mf9PTvzEK%2FkTEOP7tvNgUHVulULDjLPmLcbmdJjNc3V3sC2jf8csce4qb3TMP3abL8ACg7%2FByQd8%2BRbyCMBLdJSaVOYzDA%2FaHlA733xhR2jgTh2udyG1G0WBoOrL2Bi5UFPoQhlmUivz7ei1K5Ey%2FkoWL3zjYDaOsTtG29diXJRTAQlrUtGdGqaWmEn1r3CT%2FBmKHOeBTGS8AQONPm75mu2L0tuW%2FRUKDVXvVazBNB0GPX2V&X-Amz-Signature=5375807dd339e477bf8e6b19b42c1328fb28c83bdabc7b667fe7d32ad507961f&X-Amz-SignedHeaders=host",
        this.fileContent,
        options
        // {
        //   headers: {
        //     "Content-Type": this.fileContent["type"]
        //     // "x-amz-tagging": "myrealtag=tagvalue"
        //   }
        // }
      );
      this.isComplete = true;
    } catch (err) {
      console.log("The error is ", err);
    }
    console.log("MMMM resp is ", uploadUrl.data["body"]["url"]);
  }
}

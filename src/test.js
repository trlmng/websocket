// This file is required by karma.conf.js and loads recursively all the .spec and framework files

const elasticsearch = require("elasticsearch");
var md5 = require("md5");

let client = new elasticsearch.Client({
  host:
    "search-trlm-elasticsearch-poc-sscgdomjuhvlbd2av6gi6zgr74.us-east-1.es.amazonaws.com",
  log: "trace"
});

async function indexdoc() {
  let status = ["trash", "draft", "active", "inactive"];
  let iou = [0, 1];
  let sc = ["Cigar", "Tobacco", "E-Liquid", "Chewing Tobacco"];
  let pn = ["Apple", "Cherry", "Berry", "Menthol"];

  let idVal = getMD5(
    new Date().toISOString() + "ma@gmail.com" + getRandomArbitrary(9999, 999999)
  );

  let a = {
    product_status: status[getRandomArbitrary(0, 3)],
    registration_id: "ffdbceab3c2f7a63df83039baa202cd0",
    intended_use_product: iou[getRandomArbitrary(0, 1)],
    sub_category: sc[getRandomArbitrary(0, 3)],
    created_by: "ma@gmail.com",
    loadid: 0,
    id: idVal,
    product_name: pn[getRandomArbitrary(0, 3)],
    brandName: "Test Brand",
    userId: "ma@gmail.com",
    establishmentNumbers: ["ET1", "ET2"]
  };
  await client.index({
    id: idVal,
    index: "trlmng_poc_product",
    type: "_doc",
    body: a
  });
}

function getRandomArbitrary(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getMD5(val) {
  //console.log(md5(val.toLowerCase()));
  return md5(val.toLowerCase());
}

let y = getRandomArbitrary(0, 1);

// console.log(y);

let i = 0;

async function testy() {
  while (i < 1000000) {
    await indexdoc();
    i++;
  }
}

testy();

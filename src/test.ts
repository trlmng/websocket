// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import "zone.js/dist/zone-testing";
import { getTestBed } from "@angular/core/testing";
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from "@angular/platform-browser-dynamic/testing";

import { Client, ConfigOptions } from "elasticsearch";
var md5 = require("md5");

declare const require: any;

let client = new Client({
  host:
    "search-trlm-elasticsearch-poc-sscgdomjuhvlbd2av6gi6zgr74.us-east-1.es.amazonaws.com",
  log: "trace"
});

let submitResult = this.client.index({
  id: id,
  index: "trlmng_poc_product",
  type: "_doc",
  body: product
});

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting()
);
// Then we find all the tests.
const context = require.context("./", true, /\.spec\.ts$/);
// And load the modules.
context.keys().map(context);

function getRandomArbitrary(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getMD5(val: string) {
  console.log(md5(val.toLowerCase()));
  return md5(val.toLowerCase());
}
